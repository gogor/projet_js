$(document).ready(function() {
  //Cette partie permet d'initialiser le Calendrier quand le document a fini de
  //charger
  $('#calendar').fullCalendar({
    selectable: true,
    navLinks: true,
    header: {
      left: 'month,agendaWeek,today',
      center: 'title',
      right: 'prevYear,prev,next,nextYear'
    },
    dayClick: function(date) {
      modal(date);
    },
    aspectRatio: 2,
    eventClick: function(calEvent, jsEvent, view) {
      editEvent(calEvent);
    },
    viewRender: remplissage_calendrier
  });

  //Ajout du changement d'état au checkbox
  $('#filled-in-box').change(changementCheckbox);
  $('#filled-in-boxEdit').change(changementCheckbox);

  //Ajout de la sauvegarde de l'événement quand on clique sur OK du modal1
  $('#ok').click(saveEvent);
  $('#annuler').click(fermerModal("#modal1"));
  $('#annulerEdit').click(fermerModal("#modalEdit"));
});


function changementCheckbox(checkbox) {
  //Cette fonction permet d'activer et désactiver les timepickers quand
  //on coche ou décoche la case journée
  if (checkbox.target.id == 'filled-in-box') {
    if ($(checkbox.target).is(':checked')) {
      $('.timeCreate').attr("disabled", "disabled");
    } else {
      $('.timeCreate').removeAttr("disabled");
    }
  } else {
    if ($(checkbox.target).is(':checked')) {
      $('.timeEdit').attr("disabled", "disabled");
    } else {
      $('.timeEdit').removeAttr("disabled");
    }
  }
}

function modal(date) {
  //Cette fonction permet de reset, initialiser et lancer le modal1,
  //elle est appellé quand on clique sur un jour du Calendrier voir initialiser
  //du Calendrier dans document.ready
  //date est l'objet recupérer
  resetModal('modal1');
  document.getElementById("date").value = date.format('L');
  document.getElementById("dateFin").value = date.format('L');
  lancerModal('#modal1');
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

function resetModal(modal) {
  //Cette fonction réinitialise le modal en fonction de l'id modal qu'on met
  // en paramètre (soit 'modal1' ou 'modalEdit')
  if (modal == 'modal1') {
    document.getElementById("title").value = '';
    document.getElementById("debut").value = '';
    document.getElementById("fin").value = '';
  } else {
    document.getElementById("titleEdit").value = '';
    document.getElementById("debutEdit").value = '';
    document.getElementById("finEdit").value = '';
  }

  //initialisation des timepickers
  $('.timepicker').pickatime({
    default: 'now', // met par défaut l'heure actuel
    twelvehour: false, // utilisation du système 24h ou 12h
    donetext: 'OK', // nom du bouton terminé
    cleartext: 'Reset', // nom du bouton réinitialiser
    canceltext: 'Annuler', // nom du bouton annuler
    autoclose: false, // automatic close timepicker
  });

  //initialisation des datepickers
  $('.datepicker').pickadate({
    selectMonths: true, // Creates a dropdown to control month
    selectYears: 2, // Creates a dropdown of 2 years to control year
    labelMonthNext: 'Mois suivant',
    labelMonthPrev: 'Mois précédent',
    labelMonthSelect: 'Selectionner le mois',
    labelYearSelect: 'Selectionner une année',
    monthsFull: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
    monthsShort: ['Jan', 'Fev', 'Mar', 'Avr', 'Mai', 'Jun', 'Jul', 'Aou', 'Sep', 'Oct', 'Nov', 'Dec'],
    weekdaysFull: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
    weekdaysShort: ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'],
    weekdaysLetter: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S'],
    today: 'Aujourd\'hui',
    clear: 'Réinitialiser',
    close: 'OK',
    format: 'dd/mm/yyyy'
  });

}

function lancerModal(modal) {
  $('.modal').modal({
    dismissible: true
  });
  $(modal).modal('open');
}

function editEvent(calEvent) {
  resetModal('modalEdit');
  $('#okEdit').off('click');
  $('#supprimer').off('click');
  $('#okEdit').on('click', function() {
    modifEvent(calEvent.id);
  });
  $('#supprimer').on('click', function() {
    delEvent(calEvent.id);
  });
  document.getElementById("titleEdit").value = calEvent.title;
  if (calEvent.allDay) {
    $('#dateEdit').attr("value", calEvent.start.format('L'));
    $('#filled-in-boxEdit').prop('checked', true);
  } else {
    let dateD = calEvent.start;
    let dateF = calEvent.end;
    $('#dateEdit').attr("value", dateD.format('L'));
    $('#filled-in-boxEdit').prop('checked', false);
    document.getElementById("debutEdit").value = dateD.format('LT');
    document.getElementById("finEdit").value = dateF.format('LT');
  }
  lancerModal('#modalEdit');
}

function modifEvent(id) {
  if ($('#filled-in-boxEdit').is(':checked')) {
    var evenement = {
      "id": id,
      "titre": $("#titleEdit").val(),
      "allDay": true,
      "start": $("#dateEdit").val(),
      "end": $("#dateEditFin").val(),
      "dateModif": jour+"/"+mois+"/"+annee+" "+heure+":"+minute+":"+seconde
    };
  } else {
    var evenement = {
      "id": id,
      "titre": $("#titleEdit").val(),
      "allDay": false,
      "start": $("#dateEdit").val() + " " + $('#debutEdit').val(),
      "end": $("#dateEditFin").val() + " " + $('#finEdit').val(),
      "dateModif": jour+"/"+mois+"/"+annee+" "+heure+":"+minute+":"+seconde
    };
  }
  $.ajax({
    url: location.href + 'tasks/' + id,
    type: 'PUT',
    contentType: 'application/json',
    data: JSON.stringify(evenement),
    dataType: 'json',
    success: function(data) {
      alert('edit Success');
    },
    error: function(err) {
      alert('edit Error');
    }
  });
  remplissage_calendrier();
}

function delEvent(id) {
  $.ajax({
    url: location.href + 'tasks/' + id,
    type: 'DELETE',
    dataType: 'json',
    success: function(data) {
      alert('delete Success');
    },
    error: function(err) {
      alert('delete Error');
    }
  });
  $('#calendar').fullCalendar('removeEvents',id);
}


function saveEvent() {
  if ($('#filled-in-box').is(':checked')) {
    var evenement = {
      "titre": $("#title").val(),
      "allDay": true,
      "start": $("#date").val(),
      "end": $("#dateFin").val(),
      "dateModif": jour+"/"+mois+"/"+annee+" "+heure+":"+minute+":"+seconde
    };
  } else {
    var evenement = {
      "titre": $("#title").val(),
      "allDay": false,
      "start": $("#date").val() + " " + $('#debut').val(),
      "end": $("#dateFin").val() + " " + $('#fin').val(),
      "dateModif": jour+"/"+mois+"/"+annee+" "+heure+":"+minute+":"+seconde
    };
  }
  $.ajax({
    url: location.href + 'tasks/',
    type: 'POST',
    contentType: 'application/json',
    data: JSON.stringify(evenement),
    dataType: 'json',
    success: function(msg) {
      alert('Save Success');
    },
    error: function(err) {
      alert('Save Error');
    }
  });
  remplissage_calendrier();
}

function fermerModal(modal){
  $(modal).modal('close');
}



function remplissage_calendrier() {
  $.ajax({
    url: location.href + 'tasks/',
    type: "GET",
    dataType: "json",
    success: function(tasks) {
      for (var i = 0; i < tasks.length; i++) {
        if (tasks[i].allDay == true) {
          $('#calendar').fullCalendar('renderEvent', {
            id: tasks[i].id,
            title: tasks[i].titre,
            start: moment(tasks[i].start, 'DD/MM/YYYY'),
            end: moment(tasks[i].end, 'DD/MM/YYYY HH:mm').add(1,"d"),
            allDay: true
          });
        } else {
          $('#calendar').fullCalendar('renderEvent', {
            id: tasks[i].id,
            title: tasks[i].titre,
            start: moment(tasks[i].start, 'DD/MM/YYYY HH:mm'),
            end: moment(tasks[i].end, 'DD/MM/YYYY HH:mm')
          });
          } else {
            $('#calendar').fullCalendar('renderEvent', {
              id: tasks[i].id,
              title: tasks[i].titre,
              start: moment(tasks[i].start, 'DD/MM/YYYY HH:mm'),
              end: moment(tasks[i].end, 'DD/MM/YYYY HH:mm')
            });
          }
        }
      }
    },
    error: function(req, status, err) {
      alert("Impossible de récupérer les taches à réaliser !");
    }
  }).done(actualiser_MAJ);
}

setInterval(function(){
   remplissage_calendrier();
}, 5000);
